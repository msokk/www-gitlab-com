---
layout: markdown_page
title: "Graphic Designer"
---

## Responsibilities

* Set the house style of GitLab
* Improve the looks of GitLab the application
* Design a coherent brand image (logo's, icons, colors, typography, etc.)
* Improve the design and layout of [our static website](https://about.gitlab.com/)
* Design our marketing materials
* Design our swag (t-shirts, stickers, etc.)
* Design for special occasions (product releases, events, etc.)
* Create exciting marketing materials
* Create printed materials (business cards, banners, brochures)
* Create web advertisements

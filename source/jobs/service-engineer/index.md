---
layout: markdown_page
title: "Service Engineer Responsibilities and Tasks"
---

## Responsibilities

* Engage with our customers—anything from a small advertising firm or a university, to Fortune 100 clients.
* Communicate via email and video conferencing with potential and current clients.
* Maintain GitLab.com and our other services.
* Participate in the rotating "on-call" list to provide 24/7 emergency response availability .
* Ensure that everything we learn from running GitLab.com is set as default or communicated to our users.

Tasks in order of priority:

1. Emergency tickets/GitLab.com downtime
1. Scheduled calls/maintenance
1. Normal tickets and GitLab.com maintenance
1. Social questions piped into Zendesk (see the list below)
1. Engage with the rest of the GitLab and core team to improve GitLab
1. Fix problems, add features, improve documentation, and polish the website
1. IRC questions

Social questions piped into ZenDesk in order priority:

1. Disqus responses on blog articles
1. Twitter mentions
1. GitLab.com support forum
1. [Feature request forum](http://feedback.gitlab.com/forums/176466-general)
1. GitLab CE/EE/Omnibus issue trackers
1. GitLab mailinglist
1. [GitLab Forum](https://forum.gitlab.com/)
1. [StackOverflow tagged questions](http://stackoverflow.com/questions/tagged/gitlab)

[Internal issue to pipe the social questions into ZenDesk](https://dev.gitlab.org/gitlab/organization/issues/306)